package com.example.assignment11

import android.content.ClipData
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment11.databinding.FragmentGalleryBinding
import com.example.assignment11.databinding.ModelBinding
import java.util.*
import kotlin.collections.ArrayList

class ItemAdapter(private val imgGallery: ArrayList<Item>): RecyclerView.Adapter<ItemAdapter.ImageViewHolder>(),
    Filterable {

    var imgGaleriFilteredList = ArrayList<Item>()

    init {
        imgGaleriFilteredList = imgGallery
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ImageViewHolder {
        return ImageViewHolder(ModelBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val model = imgGaleriFilteredList[position]
        val itemPosition = position + 1

        holder.image.setImageResource(model.imgSrc)
        holder.title.text = model.title
        holder.description.text = model.description

        holder.image.setOnClickListener{ view ->
            val tit = holder.title.text.toString()
            val des = holder.description.text.toString()
            val img = model.imgSrc
            val action = GalleryDirections.actionGalleryToImageInfo(tit, des, img)
           view.findNavController().navigate(action)
        }

    }

    override fun getItemCount() = imgGaleriFilteredList.size
    inner class ImageViewHolder(private val binding: ModelBinding) : RecyclerView.ViewHolder(binding.root) {
        val image: ImageView = binding.imgImage
        val description: TextView = binding.tvDescription
        val title:TextView = binding.tvTitle
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    imgGaleriFilteredList = imgGallery
                } else {
                    val resultList = ArrayList<Item>()
                    for (row in imgGallery) {

                        if (row.title.lowercase(Locale.ROOT)
                                .contains(charSearch.lowercase(Locale.ROOT))
                        ) {
                            resultList.add(row)
                        }
                    }
                    imgGaleriFilteredList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = imgGaleriFilteredList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                imgGaleriFilteredList = results?.values as ArrayList<Item>
                notifyDataSetChanged()
            }

        }
    }
}