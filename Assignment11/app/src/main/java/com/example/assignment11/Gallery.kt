package com.example.assignment11

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import androidx.core.content.ContextCompat.getSystemService
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.assignment11.databinding.FragmentGalleryBinding
import android.os.IBinder




// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Gallery.newInstance] factory method to
 * create an instance of this fragment.
 */
class Gallery : Fragment() {

    private var _binding: FragmentGalleryBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: ItemAdapter
    private lateinit var items: ArrayList<Item>
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentGalleryBinding.inflate(inflater, container, false)
        binding.imgSearch.setBackgroundColor(Color.WHITE)

        items = ArrayList<Item>()

        binding.imgSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }

        })

        init()
        binding.imgSearch.setOnKeyListener{view, keyCode, _ ->
                 handleKeyEvent(view, keyCode)
        }
        return binding.root
    }

    private fun init () {
        setData()
        binding.recyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
        adapter = ItemAdapter(items)
        binding.recyclerView.adapter = adapter
        //    adapter.callback = { item, position ->
        //       editItem(position)
        //    }
    }
    private fun setListener() {
        GalleryDirections.actionGalleryToImageInfo()
        findNavController().navigate(R.id.action_gallery_to_imageInfo)
    }
    private fun setData() {
        items.add(Item("sea1", "I\'m a description1",  R.mipmap.image1))
        items.add(Item("sea and boat", "I\'m a description2",  R.mipmap.image2))
        items.add(Item("sea3", "I\'m a description3",  R.mipmap.image3))
        items.add(Item("boat", "I\'m a description4",  R.mipmap.image4))
        items.add(Item("bridge", "I\'m a description5", R.mipmap.image5))
        items.add(Item("mountain", "I\'m a description6", R.mipmap.image6))
        items.add(Item("sea and mountain", "I\'m a description7",  R.mipmap.image7))
        items.add(Item("splash", "I\'m a description8",  R.mipmap.image8))
        items.add(Item("purple sea", "I\'m a description9", R.mipmap.image9))
        items.add(Item("title10", "I\'m a description10", R.mipmap.image10))
    }
    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager = requireView()
                .context.
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }

    fun removePhoneKeypad() {
        val inputManager = requireView()
            .context
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val binder = requireView().windowToken
        inputManager.hideSoftInputFromWindow(
            binder,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}